# 如何搭建一个静态网站
以下命令均以root用户为登录用户：
## 第一步、安装nginx，命令如下：
```
yum install -y nginx
```
如图：
![avatar](./imgs/edu01.png)


## 第二步、设置nginx开机启动和nginx现在启动，命令如下：
```
systemctl enable nginx

systemctl start nginx
```


## 第三步、查看nginx情况，命令如下：
```
systemctl status nginx
```
如图：
![avatar](./imgs/edu02.png)
如上图，则nginx已经OK


## 第四步、在/var/下创建www文件夹，并在www文件夹下创建以域名为名称的文件夹，由于我的域名是9ihub.com，所以，创建的文件夹名称就为9ihub.com,命令分别如下：
```
mkdir /var/www

cd  /var/www

mkdir 9ihub.com

cd 9ihub.com

```
最终效果应该为下图所示：
![avatar](./imgs/edu03.png)

## 第五步、在XShell界面，点击如下红线框起来的图标：
![avatar](./imgs/edu04.png)

## 第五步、在上一步弹出的界面，将自己写的index.html文件上传到如下图所示的路径，如下图所示：
![avatar](./imgs/edu05.png)

## 第六步、在/etc/nginx/conf.d/目录下，创建一个以域名为名称的配置文件，命令如下
```
vim /etc/nginx/conf.d/9ihub.com.conf
```
如下图所示（ps：如果提示没有vim命令，则使用："yum -y install vim"命令安装）：
![avatar](./imgs/edu06.png)

## 第七步、在上一步的基础上，按i键进入插入模式，输入如下文本后，按左上角ESC键进入命令模式，按冒号进入底层命令行模式后，输入 wq!保存退出，修改nginx网站配置后，记得执行`nginx -t`测试配置文件是否正确，返回success表示正确，那么就可以执行`systemctl reload nginx`或`nginx -s reload`命令重新加载配置；如果测试不正确，请按照提示重新修改配置文件，如下：
```
server{
        listen 80;  #监听的端口
                server_name 9ihub.com; #监听的域名 
                location / {
                        root  /var/www/9ihub.com ;#网站所在路径
                        index index.html; #默认的首页文件
        }
}

```
如下图：
![avatar](./imgs/edu07.png)


## 第八步，最后，在阿里云控制台，进入ECS实例列表，然后点击进入实例，在左边栏最下点击安全组，点击列表中一行的配置规则，将80端口和443端口加入允许访问列表，并保存即可。分别如下图所示：

![avatar](./imgs/edu08.png)
![avatar](./imgs/edu09.png)
![avatar](./imgs/edu10.png)
![avatar](./imgs/edu11.png)
![avatar](./imgs/edu12.png)

<font color=red size=15>最后展示的图片已经可以访问这个简单的静态网站，不过由于还未完成备案，所以不能显示相关内容。（一般需要在通过实名认证后的2-3天后，才能进行备案）</font>

